#include <iostream>
#include <string.h>
#include <Windows.h>
#include <conio.h>
#include <WinUser.h>
#include <deque>
#include <vector>
#include "SnakeFunktions.h"

#define Snakehead '0'
#define Snakelenght 100

using namespace std;
using std::cout;

int main()
{
	const int row = 25;
	const int column = 50;
	char** field = Generatefield(row, column);
	srand(time(NULL));
	int Zufallszahlx = 1 + rand() % (column - 2);
	int Zufallszahly = 1 + rand() % (row - 2);
	int Snakex = 1 + rand() % (column - 2);
	int Snakey = 1 + rand() % (row - 2);
	int Score = 0;
	
	deque<Vector2> q;
	Vector2 SnakeTailPos = { Snakey,Snakex };
	q.push_back(SnakeTailPos);

	direction currentdirection = direction::right;
	field = Generatefield(row, column);
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	COORD coord;
	coord.X = 0;
	coord.Y = 0;

	CONSOLE_CURSOR_INFO cursorInfo;
	GetConsoleCursorInfo(hConsole, &cursorInfo);
	cursorInfo.bVisible = false; // set the cursor visibility
	SetConsoleCursorInfo(hConsole, &cursorInfo);


	while (true)
	{	
		Sleep(10);
		SetConsoleCursorPosition(hConsole, coord);
		
		field[Zufallszahly][Zufallszahlx] = (char)3;

		if ((GetAsyncKeyState(VK_RIGHT) & 0x8000) != 0 && currentdirection != direction::Left)
		{
			currentdirection = direction::right;
			
		}
		else if ((GetAsyncKeyState(VK_LEFT) & 0x8000) != 0 && currentdirection != direction::right)
		{
			currentdirection = direction::Left;
			
		}
		else if ((GetAsyncKeyState(VK_DOWN) & 0x8000) != 0 && currentdirection != direction::Top)
		{
			currentdirection = direction::down;
		}
		else if ((GetAsyncKeyState(VK_UP) & 0x8000) != 0 && currentdirection != direction::down)
		{
			currentdirection = direction::Top;
		}

		if (currentdirection == direction::right)
		{
			field[Snakey][Snakex] = ' ';
			Snakex = Snakex++;	
			if (Snakex == column - 1)
			{
				Snakex = 1;
			}	
		}
		if (currentdirection == direction::Left)
		{
			field[Snakey][Snakex] = ' ';
			Snakex = Snakex--;	
			if (Snakex == 0)
			{
				Snakex = column - 2;
			}	
		}
		
		if (currentdirection == direction::Top)
		{
			field[Snakey][Snakex] = ' ';
			Snakey = Snakey--;
			if (Snakey == 0)
			{
				Snakey = row - 2;
			}
		}
		if (currentdirection == direction::down)
		{
			field[Snakey][Snakex] = ' ';
			Snakey = Snakey++;
			if (Snakey == row - 1)
			{
				Snakey = 1;
			}
		}
		
		if (field[Snakey][Snakex] == field[Zufallszahly][Zufallszahlx])
		{
			Zufallszahlx = 1 + rand() % (column - 2);
			Zufallszahly = 1 + rand() % (row - 2);
			Score = Score + 10;
			q.push_front(Vector2{ 1,1 });
		}
		
		Vector2 SnakeTailPos = { Snakey,Snakex };
		q.push_back(SnakeTailPos);
		Vector2 Deltail = q.front();
		q.pop_front();
		field[Deltail.x][Deltail.y] = ' ';
		Vector2 Firstpos = q.back();
		
		for (int qindex = 0; qindex < q.size(); qindex++)
		{
			Vector2 currentvector = q[qindex];
			field [currentvector.x][currentvector.y]= '0';

			if (currentvector.y == Firstpos.y && currentvector.x == Firstpos.x && qindex != q.size()-1)
			{
				int end = 0;
				Clear();
				cout << "Verloren" << endl;
				cout << "Score : " << Score;
				cin >> end;
				return EXIT_SUCCESS;
			}
		}
		field[Firstpos.x][Firstpos.y] = (char)4;
	
		for (int x = 0; x < row; x++)
		{
			for (int y = 0; y < column; y++)
			{
				cout << field[x][y];
			}
			cout << endl;
		}
		cout << "Score : " << Score;
	}

}
